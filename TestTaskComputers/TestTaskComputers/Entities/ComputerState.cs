﻿namespace TestTaskComputers.Entities
{
    public enum ComputerState
    {
        TurnedOn,
        TurnedOff
    }
}
