﻿namespace TestTaskComputers.Entities
{
    public class Motherboard
    {
        public string MotherboardName { get; set; }
        public bool IsBroken { get; set; }
    }
}
