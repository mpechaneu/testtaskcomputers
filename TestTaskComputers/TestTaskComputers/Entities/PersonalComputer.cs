﻿using System.Collections.Generic;

namespace TestTaskComputers.Entities
{
    public class PersonalComputer
    {
        public int ComputerId { get; set; }
        public HashSet<ComputerProgram> InstalledPrograms { get; }
        public ComputerState State { get; set; }
        public Motherboard Motherboard { get; set; }

        public PersonalComputer()
        {
            InstalledPrograms = new HashSet<ComputerProgram>();
            State = ComputerState.TurnedOff;
        }
    }
}
