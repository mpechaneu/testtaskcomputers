﻿using System;
using TestTaskComputers.Entities;
using TestTaskComputers.Services;

namespace TestTaskComputers
{
    class Program
    {
        // Notes:
        // abstraction level: 
        //     - i didn't implement compatibility motherboard and system unit.
        //     - computer can be broken only because of motherboard.
        //     - ScrapService collects only motherboards.
        //     - Everything happens immediately (changing motherboard, installing, rebooting ect.)

        // pros & cons
        // pros: simple and nice solution. I used SOLID principles to create architecture that can be easily modified.
        // cons: it is single thread soluction.
        // for instance, while we repair computer we can start installing a programm on the next computer.
        // So if we assume that Everything doesn't happen immediately this program can be improved to multithreaded solution.

        // entities
        // ComputerProgram - program that can be installed on a Personal Computer
        // ComputerState - is computer TurnedOn/TurnedOff?
        // Motherboard - motherboard for computer
        // PersonalComputer - computer

        // services
        // DataService is responsible for storing data - computers, programms, spare motherboards.
        // InstallProgramService is responsible for installing programs.
        // RebootService is responsible for turning on, turning off and rebooting computer.
        // RepairService is responsible for changing motherboard if its possible.
        // ScrapService is responsible for collecting broken motherboards.
        // SellComputerService is responsible for selling computers.

        static void Main(string[] args)
        {
            try
            {
                // service initialize
                var scrabService = new ScrapService();
                var rebootService = new RebootService();
                var dataService = InitializeDataService(scrabService);
                var repairService = new RepairService(dataService, rebootService);
                var installService = new InstallProgramService(dataService, rebootService, repairService);
                scrabService.AddScrapProvider(installService);
                var sellService = new SellComputerService(rebootService, dataService);
                // actions
                installService.InstallProgram(dataService.GetProgram("MS Word"));
                sellService.SellComputer(dataService.GetComputer(1));
                rebootService.TurnOff(dataService.GetComputer(2));
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error:");
                Console.WriteLine(exception.Message);
            }

            Console.ReadLine();
        }

        private static DataService InitializeDataService(ScrapService scrabService)
        {
            var dataService = new DataService();
            scrabService.AddScrapProvider(dataService);
            var word = new ComputerProgram { ProgramName = "MS Word" };
            dataService.AddProgram(word);
            dataService.AddProgram(new ComputerProgram { ProgramName = "MS Excel" });
            //dataService.AddProgram(new ComputerProgram { ProgramName = "MS Excel" });
            dataService.AddComputer(new PersonalComputer
            {
                Motherboard = new Motherboard { IsBroken = false, MotherboardName = "Gigabyte Z370 Aorus Gaming 7" },
                ComputerId = 1,
                State = ComputerState.TurnedOn
            });
            dataService.AddComputer(new PersonalComputer
            {
                Motherboard = new Motherboard { IsBroken = false, MotherboardName = "MSI Z370 Godlike Gaming" },
                ComputerId = 2,
                State = ComputerState.TurnedOff
            });
            dataService.AddComputer(new PersonalComputer
            {
                Motherboard = new Motherboard { IsBroken = true, MotherboardName = "ECS Z270-Lightsaber" },
                ComputerId = 3,
                State = ComputerState.TurnedOff
            });
            dataService.AddComputer(new PersonalComputer
            {
                Motherboard = new Motherboard { IsBroken = false, MotherboardName = "Biostar Racing Z270GT9" },
                ComputerId = 4,
                State = ComputerState.TurnedOn,
                InstalledPrograms = { word }
            });
            dataService.AddMotherboard(new Motherboard() { IsBroken = false, MotherboardName = "MSI Z270 SLI Plus" });
            dataService.AddMotherboard(new Motherboard() { IsBroken = true, MotherboardName = "ASRock Fatal1ty Z170 Gaming K4" });
            return dataService;
        }
    }
}
