﻿using System;
using TestTaskComputers.Entities;

namespace TestTaskComputers.Services
{
    public class SellComputerService
    {
        private readonly IRebootService _rebootService;
        private readonly IDataService _dataService;

        public SellComputerService(IRebootService rebootService, IDataService dataService)
        {
            _rebootService = rebootService;
            _dataService = dataService;
        }

        public void SellComputer(PersonalComputer personalComputer)
        {
            _rebootService.TurnOff(personalComputer);
            _dataService.RemoveComputer(personalComputer);
            Console.WriteLine("Computer {0} has been sold out.", personalComputer.ComputerId);
        }
    }
}
