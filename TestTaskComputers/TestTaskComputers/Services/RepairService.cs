﻿using System;
using TestTaskComputers.Entities;

namespace TestTaskComputers.Services
{
    public class RepairService: IRepairService
    {
        private readonly IDataService _dataService;
        private readonly IRebootService _rebootService;

        public RepairService(IDataService dataService, IRebootService rebootService)
        {
            _dataService = dataService;
            _rebootService = rebootService;
        }

        public bool TryToChangeMotherboard(PersonalComputer computer)
        {
            _rebootService.TurnOff(computer);
            var spareMotherboard = _dataService.TryToGetMotherboard();
            if (spareMotherboard == null)
            {
                Console.WriteLine("There are no spare motherboards");
                return false;
            }
            computer.Motherboard = spareMotherboard;
            Console.WriteLine("Computer {0} has new motherboard", computer.ComputerId);
            _rebootService.TurnOn(computer);
            return true;
        }
    }
}
