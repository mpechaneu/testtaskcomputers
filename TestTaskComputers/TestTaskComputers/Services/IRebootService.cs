﻿using System;
using TestTaskComputers.Entities;

namespace TestTaskComputers.Services
{
    public interface IRebootService
    {
        void TurnOff(PersonalComputer computer);
        void TurnOn(PersonalComputer computer);
        void RebootComputer(PersonalComputer computer);
    }
}
