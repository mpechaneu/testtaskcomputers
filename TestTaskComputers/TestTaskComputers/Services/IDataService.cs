﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTaskComputers.Entities;

namespace TestTaskComputers.Services
{
    public interface IDataService
    {
        List<PersonalComputer> GetComputers();
        Motherboard TryToGetMotherboard();
        bool IsProgramAvailable(ComputerProgram program);
        void RemoveComputer(PersonalComputer computer);
    }
}
