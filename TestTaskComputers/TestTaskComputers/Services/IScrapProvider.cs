﻿using System;
using TestTaskComputers.Entities;

namespace TestTaskComputers.Services
{
    public interface IScrapProvider
    {
        event Action<Motherboard> DetectedBrokenMotherboard;
    }
}
