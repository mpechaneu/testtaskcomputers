﻿using System;
using TestTaskComputers.Entities;

namespace TestTaskComputers.Services
{
    public class RebootService: IRebootService
    {
        public void TurnOff(PersonalComputer computer)
        {
            if (computer.State == ComputerState.TurnedOff)
                return;
            Console.WriteLine("Computer {0} is turned off", computer.ComputerId);
            computer.State = ComputerState.TurnedOff;
        }

        public void TurnOn(PersonalComputer computer)
        {
            if (computer.State == ComputerState.TurnedOn)
                return;
            Console.WriteLine("Computer {0} is turned on", computer.ComputerId);
            computer.State = ComputerState.TurnedOn;
        }

        public void RebootComputer(PersonalComputer computer)
        {
            if (computer.State == ComputerState.TurnedOff)
                throw new Exception(string.Format("You cannot reboot computer that turned off. Computer id {0}", computer.ComputerId));
            Console.WriteLine("Computer {0} is rebooted", computer.ComputerId);
            computer.State = ComputerState.TurnedOn;
        }
    }
}
