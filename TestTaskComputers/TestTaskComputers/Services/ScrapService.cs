﻿using System;
using System.Collections.Generic;
using TestTaskComputers.Entities;

namespace TestTaskComputers.Services
{
    public class ScrapService
    {
        private readonly HashSet<IScrapProvider> _scrapProviders;
        private readonly HashSet<Motherboard> _brokenMotherboards;

        public ScrapService()
        {
            _brokenMotherboards = new HashSet<Motherboard>();
            _scrapProviders = new HashSet<IScrapProvider>();
        }

        public void AddScrapProvider(IScrapProvider scrapProvider)
        {
            if (_scrapProviders.Add(scrapProvider))
            {
                scrapProvider.DetectedBrokenMotherboard += NewScrapEvent;
            }
        }

        private void NewScrapEvent(Motherboard motherboard)
        {
            if (motherboard == null)
                return;
            Console.WriteLine("Added new scrap {0}", motherboard.MotherboardName);
            _brokenMotherboards.Add(motherboard);
        }
    }
}
