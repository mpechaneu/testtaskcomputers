﻿using System;
using TestTaskComputers.Entities;

namespace TestTaskComputers.Services
{
    public class InstallProgramService: IScrapProvider
    {
        private readonly IDataService _dataService;
        private readonly IRebootService _rebootService;
        private readonly IRepairService _repairService;

        public event Action<Motherboard> DetectedBrokenMotherboard;

        public InstallProgramService(IDataService dataService, IRebootService rebootService, IRepairService repairService)
        {
            _dataService = dataService;
            _rebootService = rebootService;
            _repairService = repairService;
        }
        
        public void InstallProgram(ComputerProgram program)
        {
            if (!_dataService.IsProgramAvailable(program))
                throw new Exception("You should register program in the dataServuce");
            var computers = _dataService.GetComputers();
            foreach (var personalComputer in computers)
            {
                if (IsComputerBroken(personalComputer))
                {
                    var success = _repairService.TryToChangeMotherboard(personalComputer);
                    if (!success)
                        continue;
                }
                if (HasProgram(program, personalComputer))
                {
                    Console.WriteLine("Computer {0} already has programm {1}", personalComputer.ComputerId, program.ProgramName);
                    continue;
                }
                _rebootService.TurnOn(personalComputer);
                InstallProgram(program, personalComputer);
                _rebootService.RebootComputer(personalComputer);
            }
        }

        private void InstallProgram(ComputerProgram program, PersonalComputer computer)
        {
            computer.InstalledPrograms.Add(program);
            Console.WriteLine("Programm {0} is installed. Computer id: {1}", program.ProgramName, computer.ComputerId);
        }

        private bool HasProgram(ComputerProgram program, PersonalComputer computer)
        {
            return computer.InstalledPrograms.Contains(program);
        }

        private bool IsComputerBroken(PersonalComputer computer)
        {
            if (computer.Motherboard == null)
                return true;
            if (computer.Motherboard.IsBroken)
            {
                Console.WriteLine("Detected broken motherboard");
                if (DetectedBrokenMotherboard != null)
                {
                    DetectedBrokenMotherboard(computer.Motherboard);
                    computer.Motherboard = null;
                }
                return true;
            }
            return false;
        }
    }
}
