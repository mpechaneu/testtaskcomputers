﻿using TestTaskComputers.Entities;

namespace TestTaskComputers.Services
{
    public interface IRepairService
    {
        bool TryToChangeMotherboard(PersonalComputer computer);
    }
}
