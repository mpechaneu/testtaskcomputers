﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestTaskComputers.Entities;

namespace TestTaskComputers.Services
{
    public class DataService: IDataService, IScrapProvider
    {
        private readonly Dictionary<int, PersonalComputer> _computers;
        private readonly Dictionary<string, ComputerProgram> _availablePrograms;
        private readonly HashSet<Motherboard> _spareWorkingMotherboards;

        public event Action<Motherboard> DetectedBrokenMotherboard;

        public DataService()
        {
            _computers = new Dictionary<int, PersonalComputer>();
            _availablePrograms = new Dictionary<string, ComputerProgram>();
            _spareWorkingMotherboards = new HashSet<Motherboard>();
        }

        public PersonalComputer GetComputer(int id)
        {
            return _computers[id];
        }

        public ComputerProgram GetProgram(string programName)
        {
            return _availablePrograms[programName];
        }

        public void AddComputer(PersonalComputer computer)
        {
            ValidateNewComputer(computer);
            _computers.Add(computer.ComputerId, computer);
        }

        public void AddProgram(ComputerProgram program)
        {
            ValidateNewProgram(program);
            _availablePrograms.Add(program.ProgramName, program);
        }

        public void AddMotherboard(Motherboard motherboard)
        {
            if (motherboard.IsBroken)
            {
                if (DetectedBrokenMotherboard != null)
                {
                    DetectedBrokenMotherboard(motherboard);
                }
                return;
            }
            ValidateNewSpareMotherboard(motherboard);
            _spareWorkingMotherboards.Add(motherboard);
        }

        public List<PersonalComputer> GetComputers()
        {
            return _computers.Values.ToList();
        }

        public Motherboard TryToGetMotherboard()
        {
            if (_spareWorkingMotherboards.Count == 0)
                return null;
            var motherboard = _spareWorkingMotherboards.First();
            _spareWorkingMotherboards.Remove(motherboard);
            return motherboard;
        }

        public bool IsProgramAvailable(ComputerProgram program)
        {
            return _availablePrograms.ContainsValue(program);
        }

        public void RemoveComputer(PersonalComputer computer)
        {
            if (!_computers.Remove(computer.ComputerId))
            {
                throw new Exception(string.Format("We cannot sold computer that we do not have. Computer id: {0}", computer.ComputerId));
            }
        }

        private void ValidateNewComputer(PersonalComputer computer)
        {
            PersonalComputer instance;
            if (_computers.TryGetValue(computer.ComputerId, out instance))
            {
                throw new Exception(string.Format("Computer with id {0} is already added", computer.ComputerId));
            }
            if (_spareWorkingMotherboards.Contains(computer.Motherboard))
            {
                throw new Exception(string.Format("Computer with id {0} has motherboard from reserve", computer.ComputerId));
            }
            if (computer.State == ComputerState.TurnedOn &&
                (computer.Motherboard == null || computer.Motherboard.IsBroken))
            {
                throw new Exception(string.Format("Computer with id {0} is turned on but it has broken motherboard", computer.ComputerId));
            }
        }

        private void ValidateNewProgram(ComputerProgram program)
        {
            ComputerProgram instance;
            if (_availablePrograms.TryGetValue(program.ProgramName, out instance))
                throw new Exception(string.Format("Program with name {0} is already added", program.ProgramName));
        }

        private void ValidateNewSpareMotherboard(Motherboard motherboard)
        {
            foreach (var personalComputer in _computers)
            {
                if (personalComputer.Value.Motherboard == motherboard)
                {
                    throw new Exception("Motherboard is already used");
                }
            }
        }

    }
}
